# Alarm system

it's a mini project i made in school, using [Tinkercad](https://www.tinkercad.com) platform, 

## How it works? 

it's basically using the ultrasonic sensor
to detect an intrusion and it makes a sound on a certain Hertz value when an object comes too close.

## Connection

<p align="center">
  <img src="https://i.imgur.com/eadw19C.png"/>
</p>

## Components 

`Arduino UNO`
`Wires`
`Buzzer`
`Ultrasonic Sensor - HC-SR04`
`Breadboard`