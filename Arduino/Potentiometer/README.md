# Potentiometer Controller

it's a mini project i made in school, using [Tinkercad](https://www.tinkercad.com) platform, 


## How it works 

Using the Arduino analog input function to control the brightness of the LED light by reading input voltage values through a potentiometer.

## Connection

<p align="center">
  <img src="https://i.imgur.com/SypujUQ.png"/>
</p>

## Components 

`Arduino UNO`
`Wires`
`Breadboard`
`5x LED`
`Potentiometer`
`5x Resistors 220ohm`
